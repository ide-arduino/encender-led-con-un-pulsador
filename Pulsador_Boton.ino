//Ejemplo encender led con esp32 y pulsador

const int led=18; //Se declara el led en el pin D18 del Esp32
const int pulsador=2; //Se declara el pulsador en el pin D2 del Esp32
int estado; //se declara una variable de tipo entero 
           //(varia entre 0 y 1) para chequear el estado del pulsador

void setup() {
 pinMode(led,OUTPUT); 
 pinMode(pulsador,INPUT); 

}

void loop() 
//Chequeo del estado del pulsador

{
 estado=digitalRead(pulsador);
 if (estado==HIGH)
 {
 digitalWrite(led,HIGH); 
 }
 else
 {
 digitalWrite(led,LOW); 
 }

}
